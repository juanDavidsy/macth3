using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePieces : MonoBehaviour
{
    public int indiceX;
    public int indiceY;
    public linearTypes tipodeMovimiento;
    public bool movement = false;
    public Board myBoard;
    public colorsTypes colors;
    // Start is called before the first frame update
 // funcion que da la posicion a las pieces
    public void SetCoord(int _x, int _y, Board _board)
    {
        indiceX = _x;
        indiceY = _y;
        myBoard = _board;
    }
  // funcion que llama la corrutina que mueve que las pieces
    public void MoverPiezas(int x , int y , float tiempo)
    {
        if (!movement)
        {
          
          StartCoroutine(Movepiece(x, y, tiempo));
            
        }
    }
   // corrutina que mueve las pieces
    IEnumerator Movepiece(int _x, int _y, float timeWait)
     {
        Vector2 posicionInicial = new Vector2((int)transform.position.x, (int)transform.position.y);
        Vector2 posicionFinal = new Vector2(_x, _y);

        float currentTime = 0;
        movement = true;
        bool posicionAlcanzada = false;
        while (!posicionAlcanzada)
        {
            if(Vector3.Distance(transform.position, posicionFinal) < 0.01f)
            {
                posicionAlcanzada = true;
                transform.position = posicionFinal;
                myBoard.PositionPieces(this, _x, _y);
                movement = false;
                break;
            }
            currentTime += Time.deltaTime;
            float t = currentTime / timeWait;
            switch (tipodeMovimiento)
            {
                case linearTypes.lineal:
               
                break;
                case linearTypes.senoidal:
                t = Mathf.Sin(currentTime / t);
                break;
                case linearTypes.cosenoidal:
                t = 1 - Mathf.Cos(currentTime / t * Mathf.PI * 0.5f);
                break;
                case linearTypes.suave:
                t= Mathf.Pow(t, 2) * (3 - 2 * t);
                break;
                case linearTypes.massuave:
                t = Mathf.Pow(t, 2) * (t * (t * 6 - 15) + 10);
                break;
            }
      
          
          transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            yield return new WaitForEndOfFrame();
        }
          

    }
 // tipos de moviento
    public enum linearTypes
      { 
        lineal,
        senoidal,
        cosenoidal,
        suave,
        massuave,
      }
 // tipos de colores
    public enum colorsTypes
    {
        Rojo,
        Azul,
        Celeste,
        Negro,
        Blanco,
        Verde,
        Rosa,
        Amarillo,
    }
   
}



