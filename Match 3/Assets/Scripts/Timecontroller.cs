using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Timecontroller : MonoBehaviour
{
    public int min, seg;
    public TMP_Text tiempo;
    private float restante;
    public bool isActive;
 // asigna el tiempo
    public void Awake()
    {
        
        restante = (min * 60 ) + seg;

    }
 // si esta activo comienza el temporizador
    private void Update()
    {
        if (isActive)
        {
            restante -= Time.deltaTime;
            if (restante < 1)
            {
                SceneManager.LoadScene("GameOver");
            }
            int minTime = Mathf.FloorToInt(restante / 60);
            int segTime = Mathf.FloorToInt(restante % 60);
            tiempo.text = string.Format("{00:00}:{01:00}", minTime, segTime);
        }
       
    
    }


}
