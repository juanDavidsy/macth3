using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public bool movement= false;
    public linearTypes tipodeMovimiento;
   

    // Start is called before the first frame update
 // si es llega a un square pregunta el tag y carga determinada scena
    public void StartBoton(GameObject square)
    {
        if(square.tag == "Hielo")
        {
            SceneManager.LoadScene("Hielo");
        }
        if(square.tag == "Lava")
        {
            SceneManager.LoadScene("Lava");
        }
        if(square.tag == "Oscuridad")
        {
            SceneManager.LoadScene("Oscuridad");
        }
   
    }

   
    public void QuitBoton()
    {
        Application.Quit();
        Debug.Log("Salir");

    }
 
    public void Boton_Out()
    {
        SceneManager.LoadScene("Menu");


    }
 // mueve el alien
   public  IEnumerator Move_Alien(int x, int y,float timeWait,GameObject square)
    {
        Vector2 posicionInicial = new Vector2((int)transform.position.x, (int)transform.position.y);
        Vector2 posicionFinal = new Vector2(x, y);
 
        float currentTime = 0;
        movement = true;
        bool posicionAlcanzada = false;
        while (!posicionAlcanzada)
        {
            
            if (Vector3.Distance(transform.position, posicionFinal) < 0.01f)
            {
        
                posicionAlcanzada = true;
                transform.position = posicionFinal;
                StartBoton(square);
                movement = false;
               
                break;
             
            }
            currentTime += Time.deltaTime;
            float t = currentTime / timeWait;
            switch (tipodeMovimiento)
            {
              
                    case linearTypes.suave:
                    t = Mathf.Pow(t, 2) * (3 - 2 * t);
                    break;

               
            }
                    
               
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            yield return new WaitForEndOfFrame();
                    
                
        }

    }


  // movimiento del aire
    public enum linearTypes
    {
        
        suave,
       
    }
}
