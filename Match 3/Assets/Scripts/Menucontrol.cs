using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menucontrol : MonoBehaviour
{
    // Start is called before the first frame update
  // carga la scena de inicio
    public void StartBoton()
    {

        SceneManager.LoadScene("Inicio");
    }
 // sale de la aplicacion
    public void QuitBoton()
    {
        Application.Quit();
        Debug.Log("Salir");

    }
 // sale al menu
    public void Boton_Out()
    {
        SceneManager.LoadScene("Menus");


    }
 // entra al tutorial
    public void Tutorial()
    {
        SceneManager.LoadScene("Game");
    }


}
