using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public int indiceX;
    public int indiceY;
    public Board board;
    // Start is called before the first frame update
   // da las coordenadas a los tiles
    public void Init(int x, int y, Board myBoard)
    {
        indiceX = x;
        indiceY = y;
        board = myBoard;
    }
  // llama la funcion de guardar tile
    private void OnMouseDown()
    {
        board.FirstTileSelected(this);

    }
 // llama la funcion de guardar el segundo tile
    private void OnMouseEnter()
    {
        board.SecondTileSelected(this);
    }
 // llama la funcion de soltar tile 
    private void OnMouseUp()
    {
        board.Soltar();
    }
    // Update is called once per frame
   
}
