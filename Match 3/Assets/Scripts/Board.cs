using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Board : MonoBehaviour
{
    public int ancho;
    public int alto;
    public float margen;
   
    public GameObject prefab;
    public Tile[,] myBoard;

    public Camera camara;
    public Vector3 posicionCamera;

    public GamePieces[,] gameP;
    public GameObject[] gamePieces;

    public Tile primeraPosicion;
    public Tile segundaPosicion;

    public float tiempoDeCambio;
    public bool m_PlayerInputEnable = true;

    public TextMeshProUGUI score_C;
    public int score = 0;
    public AudioSource match;
    public GameObject doblePoints;
    public int puntajeNecesario;
    public TextMeshProUGUI moves;
    public int canMove;
    public bool interaccions;

   // se asigna el primer valor del score en el awake
    private void Awake()
    {
        score_C.text = score.ToString();
       
    }
  //se inicializa la matriz del board y se le da el aspect ratio a la camara para que se centre
    private void Start()
    {
        gameP = new GamePieces[ancho, alto];
        CreateBoard();
        Matriz();
        float ratio = Screen.width / (float)Screen.height;
        float n = Mathf.Max((ancho / 2f) / ratio + margen, (alto / 2f) + margen);
        Camera.main.GetComponent<Camera>().orthographicSize = n;
        moves.text = "Moves" + canMove.ToString();

    }

    private void Update()
    {
        if (score >= puntajeNecesario)
        {
            SceneManager.LoadScene("Win");
        }
    }
    // funcion para crear el board y emparentarlo con el objeto vacio board y desde el codigo darle sus posiciones en el metodo init del tile
    public void CreateBoard()
    {
       
        posicionCamera = new Vector3((float)ancho / 2f - 0.5f, (float)alto / 2f - 0.5f, -10);
        camara.transform.position = posicionCamera;
        myBoard = new Tile[ancho, alto];
        for(int i = 0; i < ancho; i++)
        {  
           for(int j =0; j < alto; j++)
            {
              Vector2 tempPosition = new Vector2(i, j);
              GameObject go = Instantiate(prefab, tempPosition, Quaternion.identity);
              go.transform.parent = this.transform;
              go.name = "(" + i + "," + j + ")";
              Tile tile = go.GetComponent<Tile>();
              tile.Init(i, j, this);
              myBoard[i, j] = tile;
              
            }

        }
    }
    
 // se crean las pieces y se les asigna un random range a un int y se le da ese int a la array
    GameObject RandomPieces()
    {
        int fruitsToUse = Random.Range(0, gamePieces.Length);
        GameObject dot = Instantiate(gamePieces[fruitsToUse]);
        dot.GetComponent<GamePieces>().myBoard = this;
        return dot;

    }
 //se le asigna la posicion a las pieces en la matriz bidimensional
    public void PositionPieces (GamePieces jd, int x, int y)
    {
        jd.transform.position = new Vector3(x, y, 0f);

        if (InBounds(x, y))
        {
            jd.SetCoord(x, y, this);
            gameP[x, y] = jd;
        }
    }
 // se crea la funcion de creacion de pieces en la matriz bidimensional de pieces
    public void Matriz()
    {
        List<GamePieces> addedPieces = new List<GamePieces>();

        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                if (gameP[i,j] == null)
                {
                    GamePieces gamePieces = FillRandomnAt(i, j);
                    addedPieces.Add(gamePieces);
                    gamePieces.transform.parent = this.transform;

                }

            }
        }
        bool isflied = false;
        int interacctions = 0;
        int maxinteracctions = 20;
        while (!isflied)
        {
            List<GamePieces> considencias = Find_AllMatches();
            if(considencias.Count == 0)
            {
                isflied = true;
                break;
            }
            else
            {
                considencias = considencias.Intersect(addedPieces).ToList();
                ReplacePieces(considencias);
            }
           if(interacctions > maxinteracctions)
            {
                isflied = true;
               
            }
            interacctions++;
           
        }
    
    
    }
  //metodo para reemplazar las pieces en la matriz
    void ReplacePieces(List<GamePieces> coincidencias)
    {
        foreach (GamePieces gamepiece in coincidencias)
        {
           ClearPieceAt(gamepiece.indiceX, gamepiece.indiceY);
           RefullMatrizAt(gamepiece.indiceX, gamepiece.indiceY);
        }    
       
    }
 //metodo que pide un gamepieces y se llama en el metodo matriz para rellenar el board si las pieces son nulas
    private GamePieces FillRandomnAt(int x , int y , float falseOffset = 3)
    {
        //GameObject randomnPice = Instantiate(RandomPieces(), Vector3.zero, Quaternion.identity);
        //randomnPice.GetComponent<GamePieces>().SetCoord(x, y, this);
        //PositionPieces(randomnPice.GetComponent<GamePieces>(), x, y);
        //return randomnPice.GetComponent<GamePieces>();
        GameObject piece = RandomPieces();
        PositionPieces(piece.GetComponent<GamePieces>(), x, y);
        return piece.GetComponent<GamePieces>();

    }
 //metodo que se llama en replacePieces y se encarga de crear nuevas pieces y asignarles su posicion
    private void RefullMatrizAt(int x, int y)
    {
        GameObject piece = RandomPieces();
        PositionPieces(piece.GetComponent<GamePieces>(), x, y);
        
    }
 //funcion que pide una lista y se usa en la lista que reune los matches horizontal y vertical y determina la score resivida dependiendo el valor de la lista
    public void Score(List<GamePieces> pieces)
    {
        if (pieces.Count == 3)
        {
            score = score + 10;
            match.Play();
            score_C.text = score.ToString();
        }
        if(pieces.Count == 4)
        {
            score = score + 20;
            match.Play();
            score_C.text = score.ToString();
        }
        if(pieces.Count >= 6)
        {
            score = score + 60;
            match.Play();
          
            score_C.text = score.ToString();
           
        }
        

    }
   
  //booleano que pregunta si la interaccion esta en rango y previene que se desborde la matriz
    bool InBounds(int x , int y)
    {
        if (x >= 0 && x < ancho && y >= 0 && y < alto)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
 //booleano que pregunta si la piece es vecino de la otra para que te deje interactuar
    bool IsNeighbor(Tile ini, Tile fnl)
    {
        if (Mathf.Abs(ini.indiceX - fnl.indiceX) == 1 && ini.indiceY == fnl.indiceY)
        {
            return true;
        }

        if (Mathf.Abs(ini.indiceY - fnl.indiceY) == 1 && ini.indiceX == fnl.indiceX)
        {
            return true;
        }

        return false;
    }
 //funcion que guarda el tile seleccionado si el tile era nulo
    public void FirstTileSelected(Tile firstTile)
    { 
        if (primeraPosicion == null)
        {
            primeraPosicion = firstTile;
        }
       
    
    }
 //funcion que guarda el segundo tile  si el primero era diferente a nulo y si es vecino
    public void SecondTileSelected(Tile SecondTile)
    {
        if (primeraPosicion != null && IsNeighbor(primeraPosicion, SecondTile))
        {
            segundaPosicion = SecondTile;
        }
    }

 //funcion que se encarga de soltar los tiles si los dos son diferentes a nulos y luego regresa su posicion a nulo
    public void Soltar()
    {
        if(primeraPosicion!=null && segundaPosicion != null)
        {
            CambiarTile(primeraPosicion,segundaPosicion);
        }
        primeraPosicion = null;
        segundaPosicion = null;
    }
 //funcion que llama una corutina que cambia los tiles
    public void CambiarTile(Tile inicial,Tile final)
    {
        StartCoroutine(SwicthTileRoutine(inicial, final));
        
    }
   
 // Coroutinas que cambia los tiles y toma un boleeano que determine si puede interactuar
    IEnumerator SwicthTileRoutine(Tile fTile , Tile sTile)
    {
        GamePieces firstPiece = gameP[fTile.indiceX, fTile.indiceY];
        GamePieces secondPiece = gameP[sTile.indiceX, sTile.indiceY];

      if (m_PlayerInputEnable)
      {

          firstPiece.MoverPiezas(sTile.indiceX, sTile.indiceY, tiempoDeCambio);
          secondPiece.MoverPiezas(fTile.indiceX, fTile.indiceY, tiempoDeCambio);

          yield return new WaitForSeconds(tiempoDeCambio);

          List<GamePieces> firstPieceMacth = FindAllMatches(firstPiece.indiceX, firstPiece.indiceY);
          List<GamePieces> secondPieceMacth = FindAllMatches(secondPiece.indiceX, secondPiece.indiceY);

           if(firstPieceMacth.Count == 0 && secondPieceMacth.Count == 0)
           {
             firstPiece.MoverPiezas(fTile.indiceX, fTile.indiceY, tiempoDeCambio);
             secondPiece.MoverPiezas(sTile.indiceX, sTile.indiceY, tiempoDeCambio);
             yield return new WaitForSeconds(tiempoDeCambio);
             m_PlayerInputEnable = true;

           }
           else
           {
         
                yield return new WaitForSeconds(tiempoDeCambio);



                ClearAndFullBoard(firstPieceMacth.Union(secondPieceMacth).ToList());
              
        
           }
            if (interaccions)
            {

              canMove--;
              moves.text = "Moves" + canMove.ToString();
      
            }
            if(canMove <= 0)
            {
                SceneManager.LoadScene("GameOver");
            }
      }  
      
        
    }
   // lista que encuentra los macthes universales
    List<GamePieces>FindingMacthes(int startx, int starty, Vector2 searchDirection, int minlegth = 3)
    {
        List<GamePieces> macths = new List<GamePieces>();
        GamePieces startPiece = null;
        
        if (InBounds(startx,starty))
        {
            startPiece = gameP[startx, starty];
        }

        if (startPiece != null)
        {
            macths.Add(startPiece);
        }
        else
        {
            return null;
        }

        int nextX;
        int nextY;
        int maxvalue = (ancho > alto) ? ancho : alto;

        for(int i = 1; i < maxvalue -1;i++)
        {
            nextX = startx + (int)Mathf.Clamp(searchDirection.x, -1, 1) * i;
            nextY = starty + (int)Mathf.Clamp(searchDirection.y, -1, 1) * i;
            
            if (!InBounds(nextX, nextY))
            {
                break;
            }

            GamePieces nextPiece = gameP[nextX, nextY];
           
            if(nextPiece == null)
            {
                break;
            }
            else
            {

              if(nextPiece.colors == startPiece.colors && !macths.Contains(nextPiece))
              {
                macths.Add(nextPiece);

              }
              else
              {
                break;
              }
            }
       
        
        }
       if(macths.Count >= minlegth)
        {
            return macths;
        }

        return null;
    
    }
  // lista que encuentra los macthes en vertical
        
    List<GamePieces> VerticalMatches(int starX , int starY , int minLengt = 3)
    {
        List<GamePieces> upMatches = FindingMacthes(starX, starY, Vector2.up, 2);
        List<GamePieces> downMatches = FindingMacthes(starX, starY, Vector2.down, 2);

        if(upMatches == null)
        {
            upMatches = new List<GamePieces>();
        }
   
       if(downMatches == null)
        {
            downMatches = new List<GamePieces>();
        }

        var combinedMatches = upMatches.Union(downMatches).ToList();
        return combinedMatches.Count >= minLengt ? combinedMatches : null;
       
    }
  // lista que encuentra los macthes en horizontal
    List<GamePieces> HorizontalMatches(int starX,int starY,int minlengt = 3)
    {
        List<GamePieces> rightMatches = FindingMacthes(starX, starY, Vector2.right, 2);
        List<GamePieces> leftMatches = FindingMacthes(starX, starY, Vector2.left, 2);

        if(rightMatches == null)
        {
            rightMatches = new List<GamePieces>();
        }
       
        if(leftMatches == null)
        {
            leftMatches = new List<GamePieces>();
        }

        var combinedMatches = rightMatches.Union(leftMatches).ToList();
        return combinedMatches.Count >= minlengt ? combinedMatches : null;
    
    }
 // lista que una los matches verticales y horinzontales
    List<GamePieces>FindAllMatches(int x, int y , int minLength = 3)
    {
        List<GamePieces> h_Matches = HorizontalMatches(x, y, minLength);
        List<GamePieces> v_Matches = VerticalMatches(x, y, minLength);

        if(h_Matches == null)
        {
            h_Matches = new List<GamePieces>();
        }
       
        if(v_Matches == null)
        {
            v_Matches = new List<GamePieces>();
        }

        var combinedMatches = h_Matches.Union(v_Matches).ToList();
        Score(combinedMatches);
        return combinedMatches;
       
  
    }
 //funcion que apaga el color de los tiles
    void HighLigthTileOff(int x , int y)
    {
        SpriteRenderer sr = myBoard[x, y].GetComponent<SpriteRenderer>();
        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 0);
    }
  // funcion que que se enciende el color del tile si hay macths
    void HighLigthOn(int x , int y , Color col)
    {
        SpriteRenderer sr = myBoard[x, y].GetComponent<SpriteRenderer>();
        sr.color = col;
    }
    void HighLigthMatchesAt(int x , int y)
    {
        HighLigthTileOff(x, y);
        var combinedMatches = FindAllMatches(x, y);
        if(combinedMatches.Count > 0)
        {
            foreach(GamePieces piece in combinedMatches)
            {
                HighLigthOn(piece.indiceX, piece.indiceY, gameP[piece.indiceX, piece.indiceY].GetComponent<SpriteRenderer>().color);
            }
        }
       
    }
   
   //metodo sobrecardo 
    List<GamePieces> Find_AllMatches()
    {
        List<GamePieces> combinedMatches = new List<GamePieces>();
        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                var matches = FindAllMatches(i, j);
                combinedMatches = combinedMatches.Union(matches).ToList();
                

            }
        }
        return combinedMatches;
    }
  
    List<GamePieces>FindAllMatchesAt(List<GamePieces>gamePieces,int minlengt = 3)
    {
        List<GamePieces> matches = new List<GamePieces>();
        foreach(GamePieces gamePiece in gamePieces)
        {
            matches = matches.Union(FindAllMatches(gamePiece.indiceX, gamePiece.indiceY)).ToList();
           
        }
        return matches;
    }
    void ClearBoard()
    {
        for(int i = 0; i < ancho; i++)
        {
           for(int j = 0; j < alto; j++)
            {
                ClearPieceAt(i, j);
            }
        }
    }
   // metodo para destruir las pieces
    void ClearPieceAt(int x , int y)
    {
        GamePieces pieceClear = gameP[x, y];
        if(pieceClear != null)
        {
            gameP[x, y] = null;
            Destroy(pieceClear.gameObject);
        }
       // HighLigthTileOff(x, y);
    }
  // metodo que recorre las columnas
    void ClearPieceAt(List<GamePieces> gamePieces)
    {
        foreach(GamePieces piece in gamePieces)
        {
            ClearPieceAt(piece.indiceX, piece.indiceY);
        }
    }
  //lista que hace que bajen la pieces a las columnas
    List<GamePieces> CollapsColumn(int colum, float collapseTime = .1f)
    {
        List<GamePieces> movingPieces = new List<GamePieces>();
        for (int i = 0; i < alto - 1; i++)
        {
            if (gameP[colum, i] == null)
            {
                for (int j = i + 1; j < alto; j++)
                {
                    if (gameP[colum, j] != null)
                    {
                        gameP[colum, j].MoverPiezas(colum, i, collapseTime);
                        gameP[colum, j] = gameP[colum, j];
                        gameP[colum, j].SetCoord(colum, i,this);
                        if (!movingPieces.Contains(gameP[colum, i]))
                        {
                            movingPieces.Add(gameP[colum, j]);
                        }
                        gameP[colum, j] = null;
                        break;
                    }
                }
            }
        }
        return movingPieces;
    }
  // lista que se encarga de bajar las columnas
    List<GamePieces> CollapseColumn(List<GamePieces> gamePieces)
    {
        List<GamePieces> movingPieces = new List<GamePieces>();
        List<int> columnsToColapse = GetColumns(gamePieces);
        foreach(int column in columnsToColapse)
        {
            movingPieces = movingPieces.Union(CollapsColumn(column)).ToList();
        }

        return movingPieces;
    }
  // lista que toma las columnas
    List<int> GetColumns(List<GamePieces> gamePieces)
    {
        List<int> collumnIndex = new List<int>();
        foreach(GamePieces piece in gamePieces)
        {
            if (!collumnIndex.Contains(piece.indiceX))
            {
                collumnIndex.Add(piece.indiceX);
            }
        }

        return collumnIndex;
    }
  //metodo que limpia y llena el board
    public void ClearAndFullBoard(List<GamePieces> gamePieces)
    {
        StartCoroutine(ClearAndFullBoardRoutine(gamePieces));
    }

  //corutina que limpia y llena el board
    IEnumerator ClearAndFullBoardRoutine(List<GamePieces> gamePieces)
    {
        yield return StartCoroutine(ClearAndFullColumn(gamePieces));
        yield return null;
        yield return StartCoroutine(RefullRoutine());
    }
 // corrutina que se encarga de limpiar y llenar el tablero
    IEnumerator ClearAndFullColumn(List<GamePieces> gamePieces)
    {
        List<GamePieces> movingpiece = new List<GamePieces>();
        List<GamePieces> matches = new List<GamePieces>();
        bool isFinish = false;
        while (!isFinish)
        {
            ClearPieceAt(gamePieces);
            yield return new WaitForSeconds(.5f);
            movingpiece = CollapseColumn(gamePieces);
            matches = FindAllMatchesAt(movingpiece);
            while (!IsCollapsed(gamePieces))
            {
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(.5f);
            matches = FindAllMatchesAt(movingpiece);
           
            if(matches.Count == 0)
            {
                isFinish = true;
                break;
            }
            else
            {
                yield return StartCoroutine(ClearAndFullColumn(matches));
            }
        
        }
    }
 // rutina de llenar la matriz
    IEnumerator RefullRoutine()
    {
        Matriz();
        yield return null;

    }
    
 // boleeano que pregunta si esta colapsado
   bool IsCollapsed (List<GamePieces> gamePieces)
    {
        foreach(GamePieces piece in gamePieces)
        {
            if(piece != null)
            {
                if(piece.transform.position.y - (float)piece.indiceY > 0.001f)
                {
                    return false;
                }
            }
        }
        return true;
    }


 // Listas
    
    

  
    
 
  
 // Metodos de Refull , Clear y Prender tiles
    
    //void HighLigthMatches()
    //{
    //    for(int i = 0; i < ancho; i++)
    //    {
    //        for(int j = 0; j < alto; j++)
    //        {
    //            HighLigthMatchesAt(i, j);
    //        }
    //    }

    //}
    
    

   










}



